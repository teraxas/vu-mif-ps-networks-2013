package lt.mesgalis.networks.task3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Package {
	
	static public enum PackageType {
		NORMAL,
		NOTIFY;
	}
	
	private NetworkNode sender;
	private NetworkNode destination;
	
	private PackageType type;
	private Set<NetworkNode> visited = new HashSet<NetworkNode>();
	private ArrayList<Connection> path;
	

	public Package() {}
	public Package(NetworkNode sender, NetworkNode destination, PackageType type) {
		this.sender = sender;
		this.destination = destination;
		this.type = type;
	}
	
	public String getPathString() {
		String result = "";
		if (path == null) {
			return "No path!";
		}
		result += "Weight: " + Router.getWeight(getPath()) + "\n";
		for (Connection connection : path) {
			result += connection.toString() + "\n";
		}
		return result;
	}
	
	public NetworkNode getSender() {
		return sender;
	}
	public NetworkNode getDestination() {
		return destination;
	}
	public PackageType getType() {
		return type;
	}
	public Set<NetworkNode> getVisited() {
		return visited;
	}
	public ArrayList<Connection> getPath() {
		return path;
	}
	public void setSender(NetworkNode sender) {
		this.sender = sender;
	}
	public void setDestination(NetworkNode destination) {
		this.destination = destination;
	}
	public void setType(PackageType type) {
		this.type = type;
	}
	public void setVisited(Set<NetworkNode> visited) {
		this.visited = visited;
	}
	public void setPath(ArrayList<Connection> path) {
		this.path = path;
	}

}
