package lt.mesgalis.networks.task3;

import java.util.ArrayList;
import java.util.List;

public class NetworkNode {
	
	private String address;
	
	private List<Connection> connections = new ArrayList<Connection>();

	public NetworkNode() {}
	public NetworkNode(String address){
		setAddress(address);
	}
	
	public void addConnection(Connection connection) {
		connections.add(connection);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NetworkNode) {
			return ((NetworkNode) obj).getAddress().equals(address);
		}
		return false;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Connection> getConnections() {
		return connections;
	}

	public void setConnections(List<Connection> connections) {
		this.connections = connections;
	}
	

}
