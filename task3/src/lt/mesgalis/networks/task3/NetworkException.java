package lt.mesgalis.networks.task3;

public class NetworkException extends Exception {
	private static final long serialVersionUID = -6267714985832206979L;

	public NetworkException(String message) {
		super(message);
	}

}
