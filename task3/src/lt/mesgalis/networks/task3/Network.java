package lt.mesgalis.networks.task3;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Network {

	private Map<String, NetworkNode> nodes = new HashMap<String, NetworkNode>();
	
	public Network() {}
	public Network(Document doc) {
		parseXML(doc);
	}
	
	public void step() throws NetworkException {
		for (NetworkNode node : nodes.values()) {
			if (node instanceof Router) {
				((Router) node).step();
			}
		}
	}
	
	public void addRouter(String address) throws NetworkException {
		if (nodes.put(address, new Router(address)) != null){
			throw new NetworkException("address already exists: " + address);
		}
	}
	
	public void removeNode(String address) throws NetworkException {
		NetworkNode node = getNodeByAddress(address);
		if (node == null) {
			throw new NetworkException("ERROR: no such node");
		}
		
		nodes.remove(node);
		for (Connection connection : node.getConnections()) {
			NetworkNode target = connection.getTarget(node);
			target.getConnections().remove(connection);
			if (target instanceof Router) {
				((Router) target).notifyBroadcast();
			}
		}
	}
	
	public Connection addConnection(String addressA, String addressB, int weight) throws NetworkException {
		NetworkNode nodeA = getNodeByAddress(addressA);
		NetworkNode nodeB = getNodeByAddress(addressB);
		
		if (nodeA == null) {
			throw new NetworkException("node does not exist: " + addressA);
		}
		if (nodeB == null) {
			throw new NetworkException("node does not exist: " + addressB);
		}
		
		Connection connection = new Connection(nodeA, nodeB, weight);
		nodeA.addConnection(connection);
		nodeB.addConnection(connection);
		return connection;
	}
	
	public void addConnectionRunning(String addressA, String addressB, int weight) throws NetworkException {
		Connection connection = addConnection(addressA, addressB, weight);
		if (connection.getNodeA() instanceof Router) {
			((Router) connection.getNodeA()).notifyBroadcast();
		}
		if (connection.getNodeB() instanceof Router) {
			((Router) connection.getNodeB()).notifyBroadcast();
		}
	}
	
	public void removeConnection(Connection connection) {
		connection.getNodeA().getConnections().remove(connection);
		connection.getNodeB().getConnections().remove(connection);
		if (connection.getNodeA() instanceof Router) {
			((Router) connection.getNodeA()).notifyBroadcast();
		}
		if (connection.getNodeB() instanceof Router) {
			((Router) connection.getNodeB()).notifyBroadcast();
		}
	}

	public void parseXML(Document doc) {
		nodes.clear();
		
		//Nodes
		NodeList nList = doc.getElementsByTagName("router");
		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;
				String routerAddress = getTagValue("address", eElement);
				System.out.println("Adding router. Address: " + routerAddress);
				try {
					this.addRouter(routerAddress);
				} catch (NetworkException e) {
					System.out.println("Adding router " + routerAddress + " failed. Error: " + e.getMessage());
				}
			}
		}
		
		//Connections
		nList = doc.getElementsByTagName("connection");
		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;
				String addressA = getTagValue("addressA", eElement);
				String addressB = getTagValue("addressB", eElement);
				int weight = Integer.parseInt(getTagValue("weight", eElement));

				System.out.println("Adding connection between: " + addressA + " and " + addressB + ". Weight: " + weight);

				try {
					addConnection(addressA, addressB, weight);
				} catch (NetworkException e) {
					System.out.println("Adding connection from " + addressA + " to " + addressB + " failed. Error: " + e.getMessage());
				}
			}
		}
	}
	
	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}
	
	public NetworkNode getNodeByAddress(String address) {
		return nodes.get(address);
	}

	public Map<String, NetworkNode> getNodes() {
		return nodes;
	}
	
}
