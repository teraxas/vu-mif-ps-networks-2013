package lt.mesgalis.networks.task3;

import java.io.IOException;

import org.xml.sax.SAXException;

public class Main {
	
	public static void printUsage() {
		System.out.println("Usage: <command> <XML filename>");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			printUsage();
			return;
		}
		
		Network network = null;
		try {
			network = new Network(XMLReader.getDocumentFromFile(args[0]));
		} catch (SAXException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		UI ui = new UI(network);
		
		
	}

}
