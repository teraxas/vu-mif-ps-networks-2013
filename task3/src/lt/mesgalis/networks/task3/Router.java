package lt.mesgalis.networks.task3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lt.mesgalis.networks.task3.Package.PackageType;

public class Router extends NetworkNode {
	
	private ArrayList<Package> packages = new ArrayList<Package>();

	public Router(String address) {
		super(address);
	}
	
	public void step() throws NetworkException {
		for (Package pack : packages) {
			if (PackageType.NOTIFY.equals(pack.getType())) {
				broadcast(pack);
			} else if (PackageType.NORMAL.equals(pack.getType())) {
				if (pack.getDestination().getAddress().equals(this.getAddress())) {
					System.out.println("Router<" + this.getAddress() + ">: package received!");
					//Done!
				} else {
					send(pack);
				}
			}
		}
		packages.clear();
	}
	
	public void notifyBroadcast() {
		packages.add(new Package(null, null, PackageType.NOTIFY));
	}
	
	public void send(Package pack) throws NetworkException {
		int visitedCount = pack.getVisited().size();
		Connection nextPath = null;
		pack.getVisited().add(this);
		if (isNotification()) {
			pack.setPath(getShortestPathToTarget(pack.getDestination(), new HashSet<NetworkNode>()));
		}
		try {
			nextPath = pack.getPath().get(visitedCount);
		} catch (NullPointerException e) {
			System.out.println("Sending package failed!");
			return;
		}
		if (getConnections().contains(nextPath)) {
			nextPath.transfer(this, pack);
		} else {
			throw new NetworkException("ERROR: No such connection!");
		}
	}
	
	public void broadcast(Package pack) {
		pack.getVisited().add(this);
		for (Connection connection : getConnections()) {
			if (!pack.getVisited().contains(connection.getTarget(this))){
				connection.transfer(this, pack);
			}
		}
	}
	
	public Package newPackage(NetworkNode destination) {
		Package pack = new Package(this, destination, PackageType.NORMAL);
		pack.setPath(getShortestPathToTarget(destination, new HashSet<NetworkNode>()));
		addPackage(pack);
		return pack;
	}
	
	public void addPackage(Package pack) {
		packages.add(pack);
	}
	
	public void removePackage(Package pack) {
		packages.remove(pack);
	}
	
	public ArrayList<Connection> getShortestPathToTarget(NetworkNode target, Set<NetworkNode> visited) {
		ArrayList<Connection> result = null;
		ArrayList<Connection> candidate = null;
		visited.add(this);
		
		for (Connection connection : getConnections()) {
			if (!connection.getTarget(this).equals(target)) {
				if (connection.getTarget(this) instanceof Router && !visited.contains(connection.getTarget(this))) {
					Set<NetworkNode> newVisited = new HashSet<NetworkNode>(visited);
					candidate = new ArrayList<Connection>();
					candidate.add(connection);
					ArrayList<Connection> furtherPath = ((Router) connection.getTarget(this)).getShortestPathToTarget(target, newVisited);
					if (furtherPath != null) {
						candidate.addAll(furtherPath);
						if (result == null) {
							result = candidate;
						} else {
							if (getWeight(candidate) < getWeight(result)) {
								result = candidate;
							}
						}
					}
				}
			} else {
				if (result == null) {
					result = new ArrayList<Connection>();
					result.add(connection);
					break;
				} else {
					if (getWeight(candidate) < getWeight(result)) {
						result = candidate;
					}
				}
			}
		}
		
		return result;
	}
	
	static public int getWeight(List<Connection> path) {
		int result = 0;
		for (Connection connection : path) {
			result += connection.getWeight();
		}
		return result;
	}
	
	public boolean isNotification() {
		for (Package pack : packages) {
			if (PackageType.NOTIFY.equals(pack.getType())) {
				return true;
			}
		}
		return false;
	}

	public ArrayList<Package> getPackages() {
		return packages;
	}

	public void setPackages(ArrayList<Package> packages) {
		this.packages = packages;
	}
	
}
