package lt.mesgalis.networks.task3;

public class Connection {
	
	private NetworkNode nodeA;
	private NetworkNode nodeB;
	
	private int weight = 1;

	public Connection() {}
	public Connection(NetworkNode nodeA, NetworkNode nodeB, int weigth) {
		setNodeA(nodeA);
		setNodeB(nodeB);
		setWeight(weigth);
	}
	
	public void transfer(Router sender, Package pack) {
		NetworkNode target = getTarget(sender);
		if (target instanceof Router) {
			((Router) target).addPackage(pack); 
		}
		System.out.println(toString() + ">>> from <" + sender.getAddress() + "> to <" + getTarget(sender).getAddress() + ">");
	}
	
	public NetworkNode getTarget(NetworkNode sender) {
		if (nodeA.getAddress().equals(sender.getAddress())) {
			return nodeB;
		} else if (nodeB.getAddress().equals(sender.getAddress())) {
			return nodeA;
		} else {
			return null;
		}

	}
	
	@Override
	public boolean equals(Object obj) {
		return obj.hashCode() == this.hashCode();
	}
	
	@Override
	public String toString() {
		return "Connection (" + nodeA.getAddress() + " - " + nodeB.getAddress() + ")";
	}
	
	public NetworkNode getNodeA() {
		return nodeA;
	}
	public void setNodeA(NetworkNode nodeA) {
		this.nodeA = nodeA;
	}
	public NetworkNode getNodeB() {
		return nodeB;
	}
	public void setNodeB(NetworkNode nodeB) {
		this.nodeB = nodeB;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}

}
