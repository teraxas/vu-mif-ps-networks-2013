package lt.mesgalis.networks.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

public class UI {

	private Network network;
	private NetworkNode selectedNode;
	private Connection selectedConnection;
	
	private BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	
	public UI(Network network) {
		setNetwork(network);
		selectNode();
		uiLoop();
	}
	
	public void uiLoop() {
		while (selectAction()) {}
	}
	
	public boolean selectAction() {
		boolean run = true;
		printActions();
		int selection = 0;
		try {
			selection = Integer.parseInt(getInput());
		} catch (NumberFormatException e) {
			System.out.println("ERROR: Illegal selection!");
			return(run);
		}
		
		switch (selection) {
		case 0:
			try {
				network.step();
			} catch (NetworkException e) {
				e.printStackTrace();
			}
			break;
		case 1:
			System.out.println("Select destination: ");
			if (selectedNode instanceof Router) {
				System.out.println(((Router) selectedNode).newPackage(selectNode()).getPathString());
			}
			break;
		case 2:
			try {
				network.removeNode(selectedNode.getAddress());
			} catch (NetworkException e) {
				e.printStackTrace();
			}
			break;
		case 3:
			selectConnection(selectedNode);
			network.removeConnection(selectedConnection);
			break;
		case 4:
			printNodes();
			System.out.println("Select destination: ");
			try {
				network.addConnection(selectedNode.getAddress(), getInput(), Integer.parseInt(getInput("Weight: ")));
			} catch (NumberFormatException e) {
				System.out.println("ERROR: Illegal selection!");
				return(run);
			} catch (NetworkException e) {
				e.printStackTrace();
			}
			break;
		case 5:
			try {
				network.addRouter(getInput("Address:"));
			} catch (NetworkException e) {
				e.printStackTrace();
			}
		case 6:
			selectNode();
		case 7:
			printNodesConnections(selectedNode);
		default:
			System.out.println("ERROR: Illegal selection!");
			return (run);
		}
		return run;
	}
	
	public NetworkNode selectNode() {
		return selectNode(true);
	}
	
	public NetworkNode selectNode(boolean setCurrent) {
		printNodes();
		System.out.println("Select node:");
		NetworkNode newSelection = network.getNodeByAddress(getInput());
		if (newSelection == null) {
			System.out.println("ERROR: No such node!");
			return selectedNode;
		}
		if (setCurrent) {
			selectedNode = newSelection;
		}
		return newSelection;
	}
	
	public Connection selectConnection(NetworkNode node) {
		printNodesConnections(node);
		try {
			selectedConnection = node.getConnections().get(Integer.parseInt(getInput()));
		} catch (NumberFormatException e) {
			System.out.println("ERROR: Illegal selection!");
			return null;
		}
		return selectedConnection;
	}
	
	public void printNodesConnections(NetworkNode node) {
		System.out.println("Connections at " + node.getAddress() + ":");
		int i = 0;
		for (Connection connection : node.getConnections()) {
			System.out.println("   <" + i + "> " + connection.getNodeA().getAddress() + " - " + connection.getNodeB().getAddress() + " : " + connection.getWeight());
			i++;
		}
	}

	public void printNodes() {
		Iterator<NetworkNode> iterator = network.getNodes().values().iterator();
		System.out.println("Nodes:");
		for (int i = 0; i < network.getNodes().size(); i++) {
			System.out.println("   " + iterator.next().getAddress());
		}
	}
	
	public void printActions() {
		System.out.println("Actions at <" + selectedNode.getAddress() + ">:");
		System.out.println(" <0> Step");
		System.out.println(" <1> Send package");
		System.out.println(" <2> Remove this node: <" + selectedNode.getAddress() + ">");
		System.out.println(" <3> Remove connection from this node");
		System.out.println(" <4> Add connection from this node");
		System.out.println(" <5> Add node to network");
		System.out.println(" <6> Select node");
		System.out.println(" <7> List connections from this node");
	}
	
	private String getInput() {
		String input = null;
	    try {
			input = bufferRead.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return input;
	}
	
	private String getInput(String message) {
		System.out.println(message);
		return getInput();
	}
	
	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public NetworkNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(NetworkNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public Connection getSelectedConnection() {
		return selectedConnection;
	}

	public void setSelectedConnection(Connection selectedConnection) {
		this.selectedConnection = selectedConnection;
	}


}
