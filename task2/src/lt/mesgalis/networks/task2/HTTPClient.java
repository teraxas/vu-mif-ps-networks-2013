package lt.mesgalis.networks.task2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HTTPClient {

	private Socket socket;
	private BufferedReader reader;
	private PrintWriter writer;

	private String hostname;

	public HTTPClient(String hostname) {
		this.hostname = hostname;
	}

	public void connect() throws UnknownHostException, IOException {
		int port = 80;
		socket = new Socket(hostname, port);
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	public String getHeader() throws IOException {
		String result = "";
		try {
			writer.println("GET / HTTP/1.1");
			writer.println("Host: " + hostname);
			writer.println("Accept: */*");
			writer.println("User-Agent: Java");
			writer.println("");
			writer.flush();

			for (String line; (line = reader.readLine()) != null;) {
				 if (line.isEmpty()) {
					 break; // Stop after headers
				 }
				System.out.println(line);
				result += line;
			}
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (writer != null) {
				writer.close();
			}
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	public void getFile(String fileURI, String outputPath) throws IOException {
		File file = new File(outputPath);
		FileOutputStream out = new FileOutputStream(file);
		
		try {
			writer.println("GET " + fileURI);
			writer.println("Host: " + hostname);
			writer.println("Authorization: */*");  
			writer.println("User-Agent: Java");
			writer.println("");
			writer.flush();

			byte[] b = new byte[1024];
	        int len = 0;
	        while ((len = socket.getInputStream().read(b)) != -1) {
	            out.write(b, 0, len);
	            System.out.print('.');
	        }
			
		} finally {
//			try {
//				getMD5(file);
//			} catch (NoSuchAlgorithmException e1) {
//				e1.printStackTrace();
//			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (writer != null) {
				writer.close();
			}
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			out.close();
		}
	}
	
	public void getMD5(File input ) throws NoSuchAlgorithmException, IOException {
		byte[] chkSum = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(input.toPath()));
		System.out.println("\nChecksum: " + chkSum);
	}
	
}
