/**
 * 
 */
package lt.mesgalis.networks.task2;

import java.io.IOException;
import java.net.UnknownHostException;


/**
 * @author Karolis Jocevičius
 *
 */
public class Main {

	public static void help() {
		System.out.println(" --- USAGE ---");
		System.out.println("http-client [host]");
		System.out.println("http-client [host] [file URI] [output path]");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HTTPClient client = null;
		if (args.length > 0) {
			client = new HTTPClient(args[0]);
		} else {
			System.out.println("        ERROR : Not enough arguments: " + args.length);
			help();
			return;
		}
		
		try {
			System.out.println("CONNECTING TO : " + args[0]);
			client.connect();
		} catch (UnknownHostException e1) {
			System.out.println("        ERROR : UNKNOWN HOST");
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (args.length == 3) {
			try {
				System.out.println("  DOWNLOADING : " + args[1]);
				System.out.println("           AS : " + args[2]);
				client.getFile(args[1], args[2]);
				System.out.println("\n --- DONE! ---");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (args.length == 1){
			try {
				client.getHeader();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("        ERROR : Wrong amount of arguments: " + args.length);
			help();
			return;
		}
	}
	
}
