/*
 * Server.cpp
 *
 *  Created on: 2013 rugs. 15
 *      Author: karolis
 */

#include "Server.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

using namespace std;

void error(const char *msg) {
    //perror(msg);
    printf("%s\n", msg);
    //exit(1);
}


char* messUp(char input[]) {
	int i;
	char* output = new char[256];
	for (i=0; i < strlen(input); i++) {
		input[i] = tolower(input[i]);
		if (input[i] == 'a') output[i] = '1';
			else if (input[i] == 'b') output[i] = '2';
			else if (input[i] == 'c') output[i] = '3';
			else if (input[i] == 'd') output[i] = '4';
			else if (input[i] == 'e') output[i] = '5';
			else if (input[i] == 'f') output[i] = '6';
			else if (input[i] == 'g') output[i] = '7';
			else if (input[i] == 'h') output[i] = '8';
			else if (input[i] == 'i') output[i] = '9';
			else if (input[i] == 'j') output[i] = '0';
			else if (input[i] == 'k') output[i] = '-';
			else if (input[i] == 'l') output[i] = '=';
			else if (input[i] == 'm') output[i] = '!';
			else if (input[i] == 'n') output[i] = '@';
			else if (input[i] == 'o') output[i] = '#';
			else if (input[i] == 'p') output[i] = '$';
			else if (input[i] == 'q') output[i] = '%';
			else if (input[i] == 'r') output[i] = '^';
			else if (input[i] == 's') output[i] = '&';
			else if (input[i] == 't') output[i] = '*';
			else if (input[i] == 'u') output[i] = '(';
			else if (input[i] == 'v') output[i] = ')';
			else if (input[i] == 'w') output[i] = '_';
			else if (input[i] == 'x') output[i] = '+';
			else if (input[i] == 'y') output[i] = '~';
			else if (input[i] == 'z') output[i] = '`';
			else if (input[i] == ' ') output[i] = '/';
			else output[i] = input[i];
	}
	return output;
}

char* messDown(char input[]) {
	int i;
	char* output = new char[256];
	for (i=0; i < strlen(input); i++) {
		input[i] = tolower(input[i]);
		if (input[i] == '1') output[i] = 'a';
			else if (input[i] == '2') output[i] = 'b';
			else if (input[i] == '3') output[i] = 'c';
			else if (input[i] == '4') output[i] = 'd';
			else if (input[i] == '5') output[i] = 'e';
			else if (input[i] == '6') output[i] = 'f';
			else if (input[i] == '7') output[i] = 'g';
			else if (input[i] == '8') output[i] = 'h';
			else if (input[i] == '9') output[i] = 'i';
			else if (input[i] == '0') output[i] = 'j';
			else if (input[i] == '-') output[i] = 'k';
			else if (input[i] == '=') output[i] = 'l';
			else if (input[i] == '!') output[i] = 'm';
			else if (input[i] == '@') output[i] = 'n';
			else if (input[i] == '#') output[i] = 'o';
			else if (input[i] == '$') output[i] = 'p';
			else if (input[i] == '%') output[i] = 'q';
			else if (input[i] == '^') output[i] = 'r';
			else if (input[i] == '&') output[i] = 's';
			else if (input[i] == '*') output[i] = 't';
			else if (input[i] == '(') output[i] = 'u';
			else if (input[i] == ')') output[i] = 'v';
			else if (input[i] == '_') output[i] = 'w';
			else if (input[i] == '+') output[i] = 'x';
			else if (input[i] == '~') output[i] = 'y';
			else if (input[i] == '`') output[i] = 'z';
			else if (input[i] == '/') output[i] = ' ';
			else output[i] = input[i];
	}
	return output;
}

int main(int argc, char *argv[]) {
	int sock, newsock; //descr., returned by socket/accept sys call 
	
	int portno; // port number on which the server accepts connections
	socklen_t clilen; // size of address of the client
	int n; // return val of read/write calls
	char* buffer = new char[256]; 
	char* bufferDec = new char[256]; 
	char* bufferEnc = new char[256]; 
	
	
	// internet addresses (server, client)
	struct sockaddr_in serv_addr, cli_addr;
	
	// arguments check
	if (argc < 2) {
	    fprintf(stderr,"ERROR, no port provided\n");
	    exit(1);
	}
	
	// socket(domain, type, protocol)
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) 
	   error("ERROR opening socket");
	
	// bzero - set all values to zero
	// inits serv_addr to zeros
	bzero((char *) &serv_addr, sizeof(serv_addr));
	
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY; //host IP addr (this)
	serv_addr.sin_port = htons(portno); //htons - converts portno to network byte order
	
	if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("ERROR on binding");
		exit(1);
    }
	
	listen(sock,5);
	
	while (true) {
		clilen = sizeof(cli_addr);
		//accept - block until a client connects
		newsock = accept(sock, (struct sockaddr *) &cli_addr, &clilen);
		if (newsock < 0) {
			error("Error on ACCEPT");
		} else {
		    bzero(buffer, 256);
		    n = read(newsock, buffer, 255);
		    if (n < 0) {
		        error("ERROR reading from socket");
		    } else {
		        printf("Got: %s\n",buffer);
		        bufferEnc = messUp(buffer);
		        bufferDec = messDown(buffer);
		        n = write(newsock, bufferEnc, 255);
		        printf("Write: %s\n",bufferEnc);
		        if (n < 0) {
		            error("ERROR writing to socket");
		        } else {
		            n = write(newsock, bufferDec, 255);
		            printf("Write: %s\n",bufferDec);
		            if (n < 0) {
		                error("ERROR writing to socket");
		            }
		            n = close(newsock);
		            if (n < 0) {
		                error("ERROR on CLOSE");
		            }
		        }
		    }
		}
	}
	//close(sock);
	return 0; 
}

